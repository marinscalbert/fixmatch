# Configuration for splits
SPLIT_SEED = 0
N_SPLITS = 1
TRAIN_SIZE = 0.80
VAL_SIZE = 0.10
TEST_SIZE = 0.10

# Configuration model name
MODEL_NAME = "ResNet50"
PRETRAINED = True
CLASSES = 5

# Configuration for training
TRAINING_PARAMS = {
    "BATCH_SIZE": 16,
    "UNSUP_SUP_BATCH_RATIO": 5,
    "RESIZE_SHAPE": (128, 128),
    "SCALING": 1.,
    "SHUFFLE": True,
    "DROP_LAST": True,
    "LEARNING_RATE": 1e-4,
    "MIN_LEARNING_RATE": 1e-6,
    "COSINE_DECAY_LR_STEPS": 500,
    "PATIENCE_EARLY_STOPPING": 10,
    "EPOCHS": 500,
    "LAMBDA_SUPERVISED": 1.,
    "LAMBDA_UNSUPERVISED": 1.,
    "PROB_THRESHOLD": 0.9,
    "EMA_DECAY": 0.995,
    "DELTA_HUE": 0.25,
    "IMAGENET_PREPROCESS": PRETRAINED
}

# Configuration for validation
VALIDATION_PARAMS = {
    "BATCH_SIZE": 64,
    "RESIZE_SHAPE": (128, 128),
    "SCALING": 1.,
    "SHUFFLE": False,
    "DROP_LAST": False,
    "IMAGENET_PREPROCESS": PRETRAINED
}

# Configuration for testing
TESTING_PARAMS = {
    "BATCH_SIZE": 64,
    "RESIZE_SHAPE": (128, 128),
    "SCALING": 1.,
    "SHUFFLE": False,
    "DROP_LAST": False,
    "IMAGENET_PREPROCESS": PRETRAINED
}
