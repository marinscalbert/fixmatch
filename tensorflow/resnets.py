import tensorflow as tf
from blocks import make_basic_block_layer, make_bottleneck_layer


class ResNetTypeI(tf.keras.models.Model):

    def __init__(
            self,
            classes,
            layer_params,
            include_top=True,
            filters_init=64):
        super(ResNetTypeI, self).__init__()
        self.include_top = include_top
        self.classes = classes
        self.filters = [filters_init*(2**i) for i in range(4)]

        self.conv1 = tf.keras.layers.Conv2D(filters=self.filters[0],
                                            kernel_size=(7, 7),
                                            strides=2,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3),
                                               strides=2,
                                               padding="same")

        self.layer1 = make_basic_block_layer(filter_num=self.filters[0],
                                             blocks=layer_params[0])
        self.layer2 = make_basic_block_layer(filter_num=self.filters[1],
                                             blocks=layer_params[1],
                                             stride=2)
        self.layer3 = make_basic_block_layer(filter_num=self.filters[2],
                                             blocks=layer_params[2],
                                             stride=2)
        self.layer4 = make_basic_block_layer(filter_num=self.filters[3],
                                             blocks=layer_params[3],
                                             stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        if include_top:
            self.fc = tf.keras.layers.Dense(
                units=classes, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        if self.include_top:
            x = self.fc(x)
        return x

    def compute_output_shape(self, input_shape):
        if self.include_top:
            return (input_shape[0], self.classes)
        else:
            return (input_shape[0], 512)


class ResNetTypeII(tf.keras.Model):

    def __init__(
            self,
            classes,
            layer_params,
            include_top=True,
            filters_init=64):
        super(ResNetTypeII, self).__init__()
        self.include_top = include_top
        self.filters = [filters_init*(2**i) for i in range(4)]

        self.conv1 = tf.keras.layers.Conv2D(filters=self.filters[0],
                                            kernel_size=(7, 7),
                                            strides=2,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3),
                                               strides=2,
                                               padding="same")

        self.layer1 = make_bottleneck_layer(filter_num=self.filters[0],
                                            blocks=layer_params[0])
        self.layer2 = make_bottleneck_layer(filter_num=self.filters[1],
                                            blocks=layer_params[1],
                                            stride=2)
        self.layer3 = make_bottleneck_layer(filter_num=self.filters[2],
                                            blocks=layer_params[2],
                                            stride=2)
        self.layer4 = make_bottleneck_layer(filter_num=self.filters[3],
                                            blocks=layer_params[3],
                                            stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        if self.include_top:
            self.fc = tf.keras.layers.Dense(
                units=classes, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        if self.include_top:
            x = self.fc(x)

        return x

    def compute_output_shape(self, input_shape):
        if self.include_top:
            return (input_shape[0], self.classes)
        else:
            return (input_shape[0], 2048)


class ResNet18(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True,
            filters_init=64):
        super(ResNet18, self).__init__(
            classes=classes,
            layer_params=[2, 2, 2, 2],
            include_top=include_top,
            filters_init=filters_init)


class ResNet34(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True,
            filters_init=64):
        super(ResNet18, self).__init__(
            classes=classes,
            layer_params=[3, 4, 6, 3],
            include_top=include_top,
            filters_init=filters_init)


class ResNet50(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True,
            filters_init=64):
        super(ResNet18, self).__init__(
            classes=classes,
            layer_params=[3, 4, 6, 3],
            include_top=include_top,
            filters_init=filters_init)


class ResNet101(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True,
            filters_init=64):
        super(ResNet18, self).__init__(
            classes=classes,
            layer_params=[3, 4, 23, 3],
            include_top=include_top,
            filters_init=filters_init)


class ResNet152(ResNetTypeI):

    def __init__(
            self,
            classes,
            include_top=True,
            filters_init=64):
        super(ResNet18, self).__init__(
            classes=classes,
            layer_params=[3, 8, 36, 3],
            include_top=include_top,
            filters_init=filters_init)
