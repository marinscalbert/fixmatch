import tqdm
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.metrics import SparseCategoricalAccuracy, Mean
from tensorflow.keras.optimizers import Adam

from utils import ema_decay as ema_decay_fn
from losses import FixMatchUnsupervisedLoss
from callbacks import (
    EarlyStopping,
    CosineDecayLearningRateScehuler)
from evaluators import Evaluator


class FixMatchTrainer():

    def __init__(self):
        pass

    def train_step(
            self,
            student_model,
            teacher_model,
            optimizer,
            X_sup,
            y_sup,
            X_unsup_weak,
            X_unsup_strong,
            supervised_loss,
            unsupervised_loss,
            training_params,
            sample_weight,
            global_step):

        # Compute loss and gradients
        with tf.GradientTape() as tape:
            # Compute loss

            y_pred_student = student_model(X_sup, training=True)

            y_weak = student_model(
                X_unsup_weak, training=True)

            y_strong = student_model(
                X_unsup_strong, training=True)

            sup_loss = training_params["LAMBDA_SUPERVISED"] * supervised_loss(
                y_sup, y_pred_student, sample_weight=sample_weight)
            unsup_loss = training_params["LAMBDA_UNSUPERVISED"] * unsupervised_loss(
                y_weak, y_strong)
            total_loss = sup_loss + unsup_loss

            grads = tape.gradient(
                total_loss, student_model.trainable_variables)

            # Update student model weights
            optimizer.apply_gradients(
                zip(grads, student_model.trainable_variables))

        return total_loss, sup_loss, unsup_loss, grads, y_pred_student

    def train(
            self,
            student_model,
            teacher_model,
            train_supervised_dataset,
            val_supervised_dataset,
            train_unsupervised_dataset,
            training_params,
            validation_params):

        # Initialize metrics
        train_total_loss = Mean(name="train_total_loss")
        train_sup_loss = Mean(name="train_sup_loss")
        train_unsup_loss = Mean(name="train_unsup_loss")
        train_acc = SparseCategoricalAccuracy("train_acc")

        # Initialize losses
        supervised_loss = SparseCategoricalCrossentropy(from_logits=False)
        unsupervised_loss = FixMatchUnsupervisedLoss(
            prob_threshold=training_params["PROB_THRESHOLD"],
            from_logits=False)

        # Define optimizer
        optimizer = Adam(lr=training_params["LEARNING_RATE"])

        # Create callbacks
        cosine_decay_lr_scheduler = CosineDecayLearningRateScehuler(
            optimizer=optimizer,
            lr_init=training_params["LEARNING_RATE"],
            lr_final=training_params["MIN_LEARNING_RATE"],
            decay_steps=training_params["COSINE_DECAY_LR_STEPS"])
        early_stopping = EarlyStopping(
            model=teacher_model,
            patience=training_params["PATIENCE_EARLY_STOPPING"])

        global_step = 0
        for epoch in range(0, training_params["EPOCHS"]):

            pbar = tqdm.tqdm(
                train_supervised_dataset.dataset,
                unit="batch",
                desc="Epoch {}/{}, tot_loss: {}, sup_loss: {}, unsup_loss: {} acc: {}".format(
                    epoch+1,
                    training_params["EPOCHS"],
                    None, None, None, None),
                total=train_supervised_dataset.num_batches)

            # Training for one epoch
            unsup_iterator = iter(train_unsupervised_dataset.dataset)
            for X_sup, y_sup in pbar:

                # Get a pair of unsupervised batches respectively with weak and
                # strong augmentations
                X_unsup_weak, X_unsup_strong = next(unsup_iterator)

                if training_params["CLASS_WEIGHTS"] is not None:
                    class_weight = K.variable(
                        training_params["CLASS_WEIGHTS"])
                    sample_weight = tf.gather(
                        class_weight, K.cast(y_sup, tf.int32))

                total_loss, sup_loss, unsup_loss, grads, y_pred_student = self.train_step(
                    student_model=student_model,
                    teacher_model=teacher_model,
                    optimizer=optimizer,
                    X_sup=X_sup,
                    y_sup=y_sup,
                    X_unsup_weak=X_unsup_weak,
                    X_unsup_strong=X_unsup_strong,
                    supervised_loss=supervised_loss,
                    unsupervised_loss=unsupervised_loss,
                    training_params=training_params,
                    sample_weight=sample_weight,
                    global_step=global_step)

                self.update_teacher(
                    teacher_model,
                    student_model,
                    ema_decay=ema_decay_fn(
                        global_step, training_params["EMA_DECAY"]))

                train_total_loss.update_state(total_loss)
                train_sup_loss.update_state(sup_loss)
                train_unsup_loss.update_state(unsup_loss)
                train_acc.update_state(y_sup, y_pred_student)
                pbar.set_description(
                    "Epoch {}/{}, loss: {:.3f}, sup_loss: {:.3f}, unsup_loss: {:.3f}, acc: {:.4f}".format(
                        epoch+1,
                        training_params["EPOCHS"],
                        train_total_loss.result().numpy(),
                        train_sup_loss.result().numpy(),
                        train_unsup_loss.result().numpy(),
                        train_acc.result().numpy()))

                global_step += 1

            # Validation step
            evaluator = Evaluator()
            val_loss, val_acc = evaluator.evaluate(
                model=teacher_model,
                supervised_dataset=val_supervised_dataset,
                validation_params=validation_params)
            print("Val loss: {:.3f}, Val acc: {:.4f}".format(
                val_loss,
                val_acc))
            del evaluator

            cosine_decay_lr_scheduler.on_epoch_end(epoch+1)
            early_stopping.on_epoch_end(val_loss)
            if early_stopping.stop_training:
                break

    def update_teacher(
            self,
            teacher_model,
            student_model,
            ema_decay):
        # Weights teacher
        teachers_weights_list = teacher_model.get_weights()
        student_weights_list = student_model.get_weights()
        for i, (teacher_weights, student_weights) in enumerate(
                zip(teachers_weights_list, student_weights_list)):
            teachers_weights_list[i] = ema_decay * \
                teacher_weights+(1-ema_decay)*student_weights
        teacher_model.set_weights(teachers_weights_list)
