import tensorflow as tf
import numpy as np
import transforms


class SupervisedDataset():

    """Class implementing a tensorflow Dataset for supervised classification

    Attributes:
        augmentation (Union[WeakDataAugmentation, StrongDataAugmentation]):
            When calling on an image x, it returns an augmented version of the
            image.
        batch_size (int): Batch size.
        dataset (tf.data.Dataset): Supervised dataset.
        drop_last (bool): Whether or not to drop the last batch.
        imagenet_preprocess (bool): Apply imagenet preprocessing. Set to true,
            when using pretrained model.
        img_files (list): List of img file paths.
        labels (list): List of labels as integer.
        num_batches (int): Num batches to process for a single epoch.
        resize_shape (tuple): Resize shape (H, W).
        scaling (float): Multiplication scalar for scaling image.
        shuffle (bool): Whether or not to shuffle the data.
        training (bool): Set to true during training and false otherwise.
    """

    def __init__(
            self,
            img_files,
            labels,
            batch_size,
            resize_shape=None,
            scaling=None,
            shuffle=None,
            drop_last=False,
            imagenet_preprocess=False,
            training=False):
        """Constructor of the class SupervisedDataset

        Args:
            img_files (list): List of img file paths.
            labels (list): List of labels as integer.
            batch_size (int): Batch size.
            resize_shape (tuple, optional): Resize shape (H, W).
            scaling (float, optional): Multiplication scalar for scaling image.
            shuffle (bool, optional): Whether or not to shuffle the data.
            drop_last (bool, optional): Whether or not to drop the last batch.
            imagenet_preprocess (bool, optional): Apply imagenet preprocessing.
                Set to true when using pretrained model.
            training (bool, optional): Set to true during training and
                false otherwise.
        """
        self.img_files = img_files
        self.labels = labels
        self.batch_size = batch_size
        self.resize_shape = resize_shape
        self.scaling = scaling
        self.shuffle = shuffle
        self.drop_last = drop_last
        self.imagenet_preprocess = imagenet_preprocess
        self.training = training
        self.augmentation = transforms.StrongDataAugmentation(
            delta_hue=0.15,
            resize_shape=self.resize_shape,
            random_cropping=True,
            min_ratio_shape_random_cropping=0.85,
            random_blur=True)
        self.dataset = self.build_dataset()

        if self.drop_last:
            self.num_batches = np.floor(
                len(self.img_files)/self.batch_size).astype(int)
        else:
            self.num_batches = np.ceil(
                len(self.img_files)/self.batch_size).astype(int)

    def _generator(self):
        """Generator used for the Supervised dataset.

        Yields:
            (tuple): tuple containing:
                img_file (str): img file path.
                label (float): img label.
        """
        for i in range(len(self.img_files)):
            img_file = self.img_files[i]
            label = float(self.labels[i])
            yield img_file, label

    def read_and_preprocess(self, filepath, resize_shape):
        """Read and preprocess the image corresponding to filename.

        Args:
            filepath (str): Img file path.
            resize_shape (tuple): Resize shape (H, W).

        Returns:
            tf.Tensor: Tensor containing the image.
        """
        img = tf.io.read_file(filepath)
        img = tf.image.decode_jpeg(img)
        img = tf.cast(img, tf.float32)
        if self.scaling is not None:
            img = img*self.scaling
        if self.resize_shape is not None:
            img = tf.image.resize(img, self.resize_shape)
        return img

    def build_dataset(self):
        """Build the tf Dataset object

        Returns:
            tf.data.Dataset: Supervised dataset
        """
        dataset = tf.data.Dataset.from_generator(
            generator=self._generator,
            output_types=(tf.dtypes.string, tf.dtypes.float32))

        dataset = dataset.map(
            lambda x, y: (self.read_and_preprocess(x, self.resize_shape), y),
            num_parallel_calls=tf.data.experimental.AUTOTUNE)

        if self.training:
            dataset = dataset.map(
                lambda x, y: (self.augmentation(x), y),
                num_parallel_calls=tf.data.experimental.AUTOTUNE)

        if self.imagenet_preprocess:
            temp = tf.random.uniform([4, 32, 32, 3])
            tf.keras.applications.resnet.preprocess_input(temp)
            dataset = dataset.map(
                lambda x, y: (
                    tf.keras.applications.resnet.preprocess_input(x), y),
                num_parallel_calls=tf.data.experimental.AUTOTUNE)

        if self.shuffle:
            dataset = dataset.shuffle(
                buffer_size=2000,
                reshuffle_each_iteration=True)

        dataset = dataset.batch(
            self.batch_size,
            drop_remainder=self.drop_last)

        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

        return dataset


class TwoStreamUnsupervisedDataset():

    """Class implementing a two stream tensorflow Dataset for the FixMatch
        framework.
        Each step it generates a pair of batches containing the same images but
        with different data augmentations. One batch includes weak data
        augmentation whereas the other includes strong data augmentation.

    Attributes:
        batch_size (int): Batch size.
        dataset (tf.data.Dataset): Two stream dataset.
        delta_hue (float): Range of random hue [-delta_hue, +delta_hue]
            to apply during strong data augmentation.
        drop_last (bool): Whether or not to drop the last batch.
        imagenet_preprocess (bool): Apply imagenet preprocessing. Set to true,
            when using pretrained model.
        img_files (list): List of img file paths.
        resize_shape (tuple): Resize shape (H, W).
        scaling (float): Multiplication scalar for scaling image.
        shuffle (bool): Whether or not to shuffle the data.
        strong_transform (StrongDataAugmentation): Strong data augmentation
            object that performs strong data augmentation when calling on an
            image
        weak_transform (WeakDataAugmentation): Weak data augmentation
            object that performs strong data augmentation when calling on an
            image
    """

    def __init__(
            self,
            img_files,
            batch_size,
            resize_shape=None,
            scaling=None,
            delta_hue=None,
            shuffle=None,
            drop_last=False,
            imagenet_preprocess=False):
        """Constructor of the class TwoStreamUnsupervisedDataset

        Args:
            img_files (list): List of img file paths.
            batch_size (int): Batch size.
            resize_shape (tuple, optional): Resize shape (H, W).
            scaling (float, optional): Multiplication scalar for scaling image.
            delta_hue (float, optional): Range of random hue
                [-delta_hue, +delta_hue] to apply during strong data
                augmentation.
            shuffle (bool, optional): Whether or not to shuffle the data.
            drop_last (bool, optional): Whether or not to drop the last batch.
            imagenet_preprocess (bool, optional): Apply imagenet preprocessing.
            Set to true, when using pretrained model.
        """
        self.img_files = img_files
        self.batch_size = batch_size
        self.resize_shape = resize_shape
        self.scaling = scaling
        self.delta_hue = delta_hue
        self.shuffle = shuffle
        self.drop_last = drop_last
        self.imagenet_preprocess = imagenet_preprocess
        self.weak_transform = transforms.WeakDataAugmentation(
            delta_hue=None,
            resize_shape=self.resize_shape)
        self.strong_transform = transforms.StrongDataAugmentation(
            delta_hue=self.delta_hue,
            resize_shape=self.resize_shape,
            random_cropping=True,
            min_ratio_shape_random_cropping=0.85,
            random_blur=True)
        self.dataset = self.build_dataset()

    def _generator(self):
        """Generator used for the Supervised dataset.

        Yields:
            img_file (str): img file path.
        """
        for i in range(len(self.img_files)):
            yield self.img_files[i]

    def read_and_preprocess(self, filename, resize_shape):
        """Read and preprocess the image corresponding to filename.

        Args:
            filepath (str): Img file path.
            resize_shape (tuple): Resize shape (H, W).

        Returns:
            tuple: Tuple containing two copies of the same image in order to
                performs different data augmentation on each:
                    tf.Tensor: Tensor containing the image.
                    tf.Tensor: Tensor containing the image.
        """
        img = tf.io.read_file(filename)
        img = tf.image.decode_jpeg(img)
        img = tf.cast(img, tf.float32)
        if self.scaling is not None:
            img = img*self.scaling
        return img, img

    def build_dataset(self):
        """Build the tf Dataset object

        Returns:
            tf.data.Dataset: Two stream unsupervised dataset
        """
        dataset = tf.data.Dataset.from_generator(
            generator=self._generator,
            output_types=(tf.dtypes.string))
        dataset = dataset.map(
            lambda filename: self.read_and_preprocess(
                filename, self.resize_shape))

        dataset = dataset.map(
            lambda x1, x2: (self.weak_transform(
                x1), self.strong_transform(x2)),
            num_parallel_calls=tf.data.experimental.AUTOTUNE)

        if self.imagenet_preprocess:
            temp = tf.random.uniform([4, 32, 32, 3])  # Or tf.zeros
            tf.keras.applications.resnet.preprocess_input(temp)
            dataset = dataset.map(
                lambda x1, x2: (
                    tf.keras.applications.resnet.preprocess_input(x1),
                    tf.keras.applications.resnet.preprocess_input(x2)),
                num_parallel_calls=tf.data.experimental.AUTOTUNE)

        dataset = dataset.repeat()
        if self.shuffle:
            dataset = dataset.shuffle(
                buffer_size=10000,
                reshuffle_each_iteration=True)

        dataset = dataset.batch(
            self.batch_size,
            drop_remainder=self.drop_last)
        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

        return dataset
