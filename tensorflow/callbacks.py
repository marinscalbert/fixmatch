import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K


class ReduceLearningRateOnPlateau():

    def __init__(
            self,
            optimizer,
            patience,
            factor):
        self.optimizer = optimizer
        self.patience = patience
        self.factor = factor
        self.best_loss = np.inf
        self.has_not_improved = 0

    def on_epoch_end(
            self,
            value):
        if value <= self.best_loss:
            self.best_loss = value
            self.has_not_improved = 0
        else:
            self.has_not_improved += 1

        if self.has_not_improved == self.patience:
            lr = K.get_value(self.optimizer.lr)
            K.set_value(self.optimizer.lr, self.factor*lr)
            new_lr = K.get_value(self.optimizer.lr)
            print("Learning rate goes from {:5f} to {:.5f}".format(
                lr,
                new_lr))


class CosineDecayLearningRateScehuler():

    def __init__(
            self,
            optimizer,
            lr_init,
            lr_final,
            decay_steps):
        self.optimizer = optimizer
        self.cosine_decay = tf.keras.experimental.CosineDecay(
            initial_learning_rate=lr_init,
            decay_steps=decay_steps,
            alpha=lr_final/lr_init)

    def on_epoch_end(
            self,
            step):
        lr = K.get_value(self.optimizer.lr)
        new_lr = self.cosine_decay(step)
        K.set_value(self.optimizer.lr, new_lr)
        print("Learning rate goes from {} to {}".format(
            lr,
            new_lr))


class EarlyStopping():

    def __init__(
            self,
            model,
            patience):
        self.model = model
        self.patience = patience
        self.best_loss = np.inf
        self.has_not_improved = 0
        self.stop_training = False

    def on_epoch_end(
            self,
            value):
        if value <= self.best_loss:
            self.best_loss = value
            self.has_not_improved = 0
            self.best_weights = self.model.get_weights()
        else:
            self.has_not_improved += 1

        if self.has_not_improved == self.patience:
            print("Early stopping: loss have not improved for {} times".format(
                self.has_not_improved))
            print("Restoring best weights")
            self.model.set_weights(self.best_weights)
            self.stop_training = True
