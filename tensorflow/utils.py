def ema_decay(t, alpha=0.999):
    alpha = min(1 - 1 / (t + 1), alpha)
    return alpha


def linear_ramp(t, t_start, t_end):
    if t < t_start:
        return 0
    else:
        return min((t-t_start)/(t_end-t_start), 1)


def step_ramp(t, t_start):
    if t < t_start:
        return 0
    else:
        return 1
