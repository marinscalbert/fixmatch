import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.losses import (
    Loss,
    SparseCategoricalCrossentropy,
    Reduction)


class FixMatchUnsupervisedLoss(Loss):

    """Class implementing the unsupervised loss term used in the FixMatch paper

    Attributes:
        from_logits (bool): True if the inputs are logits. If they are
            probabilities, should be False.
        prob_threshold (float): Probability threshold hyperparameter used in
            the FixMatch paper.
        sce (SparseCategoricalCrossentropy): Sparse categorical cross entropy
            loss object used for the unsupervised loss term computation.
    """

    def __init__(
            self,
            prob_threshold=0.7,
            from_logits=False):
        """Constructor of the class FixMatchUnsupervisedLoss

        Args:
            prob_threshold (float, optional): Probability threshold
            hyperparameter used in the FixMatch paper.
            from_logits (bool, optional): True if the inputs are logits.
                If they are probabilities, should be False.
        """
        super(FixMatchUnsupervisedLoss, self).__init__(
            reduction=Reduction.AUTO, name="FixMatchUnsupervisedLoss")
        self.prob_threshold = prob_threshold
        self.from_logits = from_logits
        self.sce = SparseCategoricalCrossentropy(
            from_logits=False,
            reduction=Reduction.NONE)

    def call(self, y_weak, y_strong):
        """Computes the FixMatch unsupervised loss term


        Args:
            y_weak (tf.Tensor): Predictions from the teacher model.
            y_strong (tf.Tensor): Predictions from the student model.

        Returns:
            tf.Tensor: Loss value.
        """
        if self.from_logits:
            probs_weak = K.softmax(y_weak, axis=-1)
            probs_strong = K.softmax(y_strong, axis=-1)
        else:
            probs_weak = y_weak
            probs_strong = y_strong

        targets = K.argmax(probs_weak, axis=-1)
        max_probs = tf.gather_nd(
            probs_weak,
            K.stack(
                [tf.range(K.shape(probs_weak)[0], dtype=tf.int32),
                 K.cast(targets, tf.int32)],
                axis=1))
        probs_above_threshold = K.cast(
            max_probs > self.prob_threshold, tf.float32)

        losses = self.sce(
            y_true=targets,
            y_pred=probs_strong,
            sample_weight=probs_above_threshold)
        return losses
