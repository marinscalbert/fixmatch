# FixMatch: Simplifying Semi-Supervised Learning with Consistency and Confidence

This project provides a Tensorflow implementation of the semi-supervised framework detailed in this
<a href="https://arxiv.org/pdf/2001.07685.pdf">paper</a>.

<p align="center">
<img src="https://miro.medium.com/max/2914/1*nwxClQDqWakknowNAqTobQ.png" alt="Diagram of FixMatch" scale="1"/>
</p>

## Get started

### Set up environment

#### Via conda

1. Create the conda environment

```bash
conda create -n fixmatch python=3.7 \
	tensorflow \
	tqdm \
	scikit-learn \
```

2. Activate the conda environment

```bash
conda activate fixmatch
```

3. For GPUs users

If you have GPUs available, run instead the following command :

```bash
conda create -n fixmatch python=3.7 \
	tensorflow \
	tensorflow-gpu \
	tqdm \
	scikit-learn \
```

### Set configurations

The data dir should be specified in `constants.py`.
<br>
The configuration can be modified through the file `config.py`.

### Train a model with FixMatch

After activating the environment and setting all the configurations in `constants.py` and `config.py`. Run :
```bash
python main.py
```
