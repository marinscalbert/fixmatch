import numpy as np

import constants
import load_data
import config
import models
import datasets
import trainers
import evaluators


def main():
    # Define supervised and unsupervised dataset
    X_sup, y_sup, label2index, index2label, class_weights = load_data.get_supervised_data(
        constants.SUPERVISED_DATA_DIR)
    X_unsup = load_data.get_unsupervised_data(
        constants.UNSUPERVISED_DATA_DIR)

    # Update class weights in config
    config.TRAINING_PARAMS["CLASS_WEIGHTS"] = np.array(class_weights)
    config.VALIDATION_PARAMS["CLASS_WEIGHTS"] = np.array(class_weights)
    config.TESTING_PARAMS["CLASS_WEIGHTS"] = np.array(class_weights)

    # Compute splits
    splits = load_data.get_train_val_test_splits(
        X=X_sup,
        y=y_sup,
        n_splits=config.N_SPLITS,
        train_size=config.TRAIN_SIZE,
        val_size=config.VAL_SIZE,
        test_size=config.TEST_SIZE,
        seed=config.SPLIT_SEED)

    # Train model with FixMatch method for each split
    for split in splits:

        # Create datasets from the given splits
        train_supervised_dataset = datasets.SupervisedDataset(
            img_files=split["train"]["img_files"],
            labels=split["train"]["labels"],
            batch_size=config.TRAINING_PARAMS["BATCH_SIZE"],
            resize_shape=config.TRAINING_PARAMS["RESIZE_SHAPE"],
            scaling=config.TRAINING_PARAMS["SCALING"],
            shuffle=config.TRAINING_PARAMS["SHUFFLE"],
            drop_last=config.TRAINING_PARAMS["DROP_LAST"],
            imagenet_preprocess=config.TRAINING_PARAMS["IMAGENET_PREPROCESS"],
            training=True)

        val_supervised_dataset = datasets.SupervisedDataset(
            img_files=split["val"]["img_files"],
            labels=split["val"]["labels"],
            batch_size=config.VALIDATION_PARAMS["BATCH_SIZE"],
            resize_shape=config.VALIDATION_PARAMS["RESIZE_SHAPE"],
            scaling=config.VALIDATION_PARAMS["SCALING"],
            shuffle=config.VALIDATION_PARAMS["SHUFFLE"],
            drop_last=config.VALIDATION_PARAMS["DROP_LAST"],
            imagenet_preprocess=config.VALIDATION_PARAMS["IMAGENET_PREPROCESS"],
            training=False)

        test_supervised_dataset = datasets.SupervisedDataset(
            img_files=split["test"]["img_files"],
            labels=split["test"]["labels"],
            batch_size=config.TESTING_PARAMS["BATCH_SIZE"],
            resize_shape=config.TESTING_PARAMS["RESIZE_SHAPE"],
            scaling=config.TESTING_PARAMS["SCALING"],
            shuffle=config.TESTING_PARAMS["SHUFFLE"],
            drop_last=config.TESTING_PARAMS["DROP_LAST"],
            imagenet_preprocess=config.TESTING_PARAMS["IMAGENET_PREPROCESS"],
            training=False)

        two_stream_unsupervised_dataset = datasets.TwoStreamUnsupervisedDataset(
            img_files=X_unsup,
            batch_size=int(
                config.TRAINING_PARAMS["UNSUP_SUP_BATCH_RATIO"]*config.TRAINING_PARAMS["BATCH_SIZE"]),
            resize_shape=config.TRAINING_PARAMS["RESIZE_SHAPE"],
            scaling=config.TRAINING_PARAMS["SCALING"],
            delta_hue=config.TRAINING_PARAMS["DELTA_HUE"],
            shuffle=config.TRAINING_PARAMS["SHUFFLE"],
            drop_last=config.TRAINING_PARAMS["DROP_LAST"],
            imagenet_preprocess=config.TESTING_PARAMS["IMAGENET_PREPROCESS"],)

        # Create student and teacher models
        model_class = models.get_model_class(
            "ResNet50")
        student_model = model_class(
            classes=len(label2index),
            pretrained=config.PRETRAINED).model
        teacher_model = model_class(
            classes=len(label2index),
            pretrained=config.PRETRAINED).model
        teacher_model.set_weights(student_model.get_weights())

        # Create trainer, evaluator and testor
        trainer = trainers.FixMatchTrainer()

        # Training for semi supervised setting
        trainer.train(
            student_model=student_model,
            teacher_model=teacher_model,
            train_supervised_dataset=train_supervised_dataset,
            val_supervised_dataset=val_supervised_dataset,
            train_unsupervised_dataset=two_stream_unsupervised_dataset,
            training_params=config.TRAINING_PARAMS,
            validation_params=config.VALIDATION_PARAMS)

        evaluator = evaluators.Evaluator()
        y_true, y_pred = evaluator.evaluate(
            model=teacher_model,
            supervised_dataset=test_supervised_dataset,
            validation_params=config.TESTING_PARAMS,
            return_gt_preds=True)

        # Do whatever you want with the predictions to compute metrics ...


if __name__ == '__main__':
    main()
