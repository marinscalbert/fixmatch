import tqdm
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.losses import SparseCategoricalCrossentropy, Reduction
from tensorflow.keras.metrics import Accuracy


class Evaluator():

    def __init__(
            self):
        super(Evaluator, self).__init__()

    @tf.function
    def predict_step(
            self,
            model,
            X_sup,
            y_sup):

        y_pred = model(X_sup, training=False)
        pred_classes = K.argmax(y_pred, axis=-1)
        pred_classes = K.cast(pred_classes, tf.float32)

        return y_sup, y_pred, pred_classes

    def evaluate(
            self,
            model,
            supervised_dataset,
            validation_params,
            return_gt_preds=False):

        # Initialize metrics
        val_acc = Accuracy()

        # Initialize losses
        val_loss = SparseCategoricalCrossentropy(
            from_logits=False,
            reduction=Reduction.AUTO)

        y_true = []
        y_pred = []
        pred_classes = []
        sample_weights = []

        pbar = tqdm.tqdm(
            supervised_dataset.dataset,
            unit="batch",
            total=supervised_dataset.num_batches)
        for X_sup, y_sup in pbar:

            targets, preds, classes = self.predict_step(
                model=model,
                X_sup=X_sup,
                y_sup=y_sup)

            targets = targets.numpy()
            preds = preds.numpy()
            classes = classes.numpy()
            sample_weight = validation_params["CLASS_WEIGHTS"][targets.astype(int)]

            y_true.extend(targets.tolist())
            y_pred.extend(preds.tolist())
            pred_classes.extend(classes.tolist())
            sample_weights.extend(sample_weight.tolist())

        val_acc_value = val_acc(y_true, pred_classes)
        val_loss_value = val_loss(
            tf.convert_to_tensor(y_true),
            tf.convert_to_tensor(y_pred),
            sample_weight=tf.convert_to_tensor(sample_weights))

        if return_gt_preds:
            return y_true, pred_classes
        else:
            return val_loss_value.numpy(), val_acc_value.numpy()
