from tensorflow.keras.applications.resnet import ResNet50
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (
    GlobalAveragePooling2D,
    Dense)


def get_model_class(model_name):
    if model_name == "ResNet50":
        return CustomResNet50


class CustomResNet50():

    def __init__(
            self,
            classes,
            pretrained):
        self.classes = classes
        self.pretrained = pretrained
        self.weights = "imagenet" if pretrained else None
        self.model = self.build_model()

    def build_model(self):
        base_model = ResNet50(
            input_shape=(None, None, 3),
            weights=self.weights,
            include_top=False)
        model = Sequential(
            [base_model,
             GlobalAveragePooling2D(),
             Dense(self.classes, activation="softmax")])
        return model


if __name__ == '__main__':
    pass
