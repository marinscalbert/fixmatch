import os
import glob
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.utils.class_weight import compute_class_weight


def get_supervised_data(data_dir):
    labels = [label for label in os.listdir(
        data_dir) if not(label.startswith("."))]
    label2index = {c: i for i, c in enumerate(labels)}
    index2label = {i: l for l, i in label2index.items()}

    X = []
    y = []
    for label in labels:
        img_files = glob.glob(
            os.path.join(data_dir, label, "*.jpeg"))
        print(label, " : ", len(img_files))
        X.extend(img_files)
        y.extend(len(img_files)*[label2index[label]])

    class_weights = compute_class_weight(
        "balanced",
        classes=list(index2label.keys()),
        y=y).tolist()

    return X, y, label2index, index2label, class_weights


def get_unsupervised_data(data_dir):
    X = glob.glob(
        os.path.join(data_dir, "*.jpeg"))
    np.random.shuffle(X)
    return X


def get_train_val_test_splits(
        X,
        y,
        n_splits,
        train_size,
        val_size,
        test_size,
        seed=None):

    # Make train, val, test splits reproducible if required
    if seed:
        random_states = [seed+i for i in range(n_splits)]
    else:
        random_states = n_splits*[None]

    splits = []
    for _, random_state in zip(range(n_splits), random_states):
        X_train_val, X_test, y_train_val, y_test = train_test_split(
            X, y,
            train_size=train_size+val_size,
            test_size=test_size,
            stratify=y,
            shuffle=True,
            random_state=random_state)
        X_train, X_val, y_train, y_val = train_test_split(
            X_train_val, y_train_val,
            train_size=train_size/(train_size+val_size),
            test_size=val_size/(train_size+val_size),
            stratify=y_train_val,
            shuffle=True,
            random_state=random_state)
        splits.append(
            {"train":
                {"img_files": X_train,
                 "labels": y_train},
             "val":
                {"img_files": X_val,
                 "labels": y_val},
             "test":
                {"img_files": X_test,
                 "labels": y_test}})
    return splits


def get_train_val_splits(
        X,
        y,
        n_splits,
        train_size,
        val_size,
        seed=None):

    # Make train, val, test splits reproducible if required
    if seed:
        random_states = [seed+i for i in range(n_splits)]
    else:
        random_states = n_splits*[None]

    splits = []
    for _, random_state in zip(range(n_splits), random_states):
        X_train, X_val, y_train, y_val = train_test_split(
            X, y,
            train_size=train_size,
            test_size=val_size,
            stratify=y,
            shuffle=True,
            random_state=random_state)
        splits.append(
            {"train":
                {"img_files": X_train,
                 "labels": y_train},
             "val":
                {"img_files": X_val,
                 "labels": y_val}})
    return splits
