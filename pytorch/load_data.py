import os
import glob
import numpy as np
from sklearn.model_selection import train_test_split
from torchvision import transforms

import augmentations
import datasets


def get_supervised_data(data_dir):
    labels = [label for label in os.listdir(data_dir) if not(
        label.startswith("."))]
    label2index = {c: i for i, c in enumerate(labels)}
    index2label = {i: l for l, i in label2index.items()}

    X = []
    y = []
    for label in labels:
        img_files = glob.glob(
            os.path.join(data_dir, label, "*.jpeg"))
        X.extend(img_files)
        y.extend(len(img_files)*[label2index[label]])

    return X, y, label2index, index2label


def get_unsupervised_data(data_dir):
    X = glob.glob(
        os.path.join(data_dir, "*.jpeg"))
    np.random.shuffle(X)
    return X


def get_train_val_test_splits(
        X,
        y,
        n_splits,
        train_size,
        val_size,
        test_size,
        seed=None):

    # Make train, val, test splits reproducible if required
    if seed:
        random_states = [seed+i for i in range(n_splits)]
    else:
        random_states = n_splits*[None]

    splits = []
    for _, random_state in zip(range(n_splits), random_states):
        X_train_val, X_test, y_train_val, y_test = train_test_split(
            X, y,
            train_size=train_size+val_size,
            test_size=test_size,
            stratify=y,
            shuffle=True,
            random_state=random_state)
        X_train, X_val, y_train, y_val = train_test_split(
            X_train_val, y_train_val,
            train_size=train_size/(train_size+val_size),
            test_size=val_size/(train_size+val_size),
            stratify=y_train_val,
            shuffle=True,
            random_state=random_state)
        splits.append(
            {"train":
                {"img_files": X_train,
                 "labels": y_train},
             "val":
                {"img_files": X_val,
                 "labels": y_val},
             "test":
                {"img_files": X_test,
                 "labels": y_test}})
    return splits


def get_supervised_dataset(
        X,
        y,
        resize_shape=None,
        normalization=False,
        training=False):
    transformations = []
    if resize_shape is not None:
        transformations.append(transforms.Resize(resize_shape))

    if training:
        transformations.extend(
            [augmentations.PILRandomRotation90(),
             transforms.RandomVerticalFlip(0.5),
             transforms.RandomHorizontalFlip(0.5),
             transforms.ColorJitter(
                brightness=0,
                contrast=0,
                saturation=0,
                hue=0.10),
             augmentations.RandomGaussianBlur(
                [1, 2],
                prob=1/2)
             ])

    transformations.append(transforms.ToTensor())
    if normalization:
        transformations.append(transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]))
    dataset = datasets.SupervisedDataset(
        img_files=X,
        labels=y,
        transform=transforms.Compose(transformations))
    return dataset


def get_unsupervised_dataset(
        X_unsup,
        resize_shape=None,
        normalization=False):

    weak_transforms = []
    strong_transforms = []

    if resize_shape is not None:
        weak_transforms.append(transforms.Resize(resize_shape))
        strong_transforms.append(transforms.Resize(resize_shape))

    weak_transforms.extend([
        augmentations.PILRandomRotation90(),
        transforms.RandomVerticalFlip(0.5),
        transforms.RandomHorizontalFlip(0.5)])
    strong_transforms.extend([
        augmentations.PILRandomRotation90(),
        transforms.RandomVerticalFlip(0.5),
        transforms.RandomHorizontalFlip(0.5),
        transforms.RandomResizedCrop(
            size=resize_shape,
            scale=(0.8, 1),
            ratio=(0.8, 1.2),
            interpolation=2),
        augmentations.RandomGaussianBlur(
            [1, 2],
            prob=1/2),
        transforms.ColorJitter(
            brightness=0,
            contrast=0,
            saturation=0,
            hue=0.25)])

    weak_transforms.append(transforms.ToTensor())
    strong_transforms.append(transforms.ToTensor())

    if normalization:
        weak_transforms.append(transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]))
        strong_transforms.append(transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]))

    unsupervised_dataset = datasets.TwoStreamUnsupervisedDataset(
        img_files=X_unsup,
        weak_transform=transforms.Compose(weak_transforms),
        strong_transform=transforms.Compose(strong_transforms))

    return unsupervised_dataset
