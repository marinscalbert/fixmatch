import numbers
import numpy as np
from PIL import ImageFilter


class PILRandomRotation90():

    def __init__(self):
        pass

    def __call__(self, x):
        k = np.random.choice([0, 1, 2, 3])
        x = x.rotate(int(k*90))
        return x


class RandomGaussianBlur(object):
    def __init__(
            self,
            radius,
            prob=1/2):
        self.prob = prob
        if isinstance(radius, numbers.Number):
            self.min_radius = radius
            self.max_radius = radius
        elif isinstance(radius, list):
            if len(radius) != 2:
                raise Exception(
                    "`radius` should be a number or a list of two numbers")
            if radius[1] < radius[0]:
                raise Exception(
                    "radius[0] should be <= radius[1]")
            self.min_radius = radius[0]
            self.max_radius = radius[1]
        else:
            raise Exception(
                "`radius` should be a number or a list of two numbers")

    def __call__(self, image):
        if np.random.rand() < self.prob:
            radius = np.random.uniform(self.min_radius, self.max_radius)
            return image.filter(ImageFilter.GaussianBlur(radius))
        else:
            return image
