import json
import copy
import constants
import collections
import config
import load_data
import models
import trainers
import evaluators


def main():
    # Load images files
    X_sup, y_sup, label2index, index2label = load_data.get_supervised_data(
        constants.SUPERVISED_DATA_DIR)
    X_unsup = load_data.get_unsupervised_data(
        constants.UNSUPERVISED_DATA_DIR)

    # Create train val test splits
    splits = load_data.get_train_val_test_splits(
        X_sup,
        y_sup,
        n_splits=config.N_SPLITS,
        train_size=config.TRAIN_SIZE,
        val_size=config.VAL_SIZE,
        test_size=config.TEST_SIZE,
        seed=config.SPLIT_SEED)

    results = []

    for split in splits:

        # Create model
        model_class = models.get_model_class(config.MODEL_NAME)
        student_model = model_class(
            num_classes=config.CLASSES,
            pretrained=config.PRETRAINED,
        )
        teacher_model = copy.deepcopy(
            student_model)

        # Display composition train val test
        counter_train = collections.Counter(
            [index2label[index] for index in split["train"]["labels"]])
        counter_val = collections.Counter(
            [index2label[index] for index in split["val"]["labels"]])
        counter_test = collections.Counter(
            [index2label[index] for index in split["test"]["labels"]])
        print(index2label)
        print(counter_train)
        print(counter_val)
        print(counter_test)

        # Create datasets
        supervised_train_dataset = load_data.get_supervised_dataset(
            X=split["train"]["img_files"],
            y=split["train"]["labels"],
            resize_shape=config.TRAINING_PARAMS["resize_shape"],
            normalization=config.TRAINING_PARAMS["imagenet_normalization"],
            training=True)

        supervised_val_dataset = load_data.get_supervised_dataset(
            X=split["val"]["img_files"],
            y=split["val"]["labels"],
            resize_shape=config.VALIDATION_PARAMS["resize_shape"],
            normalization=config.VALIDATION_PARAMS["imagenet_normalization"],
            training=False)

        supervised_test_dataset = load_data.get_supervised_dataset(
            X=split["test"]["img_files"],
            y=split["test"]["labels"],
            resize_shape=config.TESTING_PARAMS["resize_shape"],
            normalization=config.TESTING_PARAMS["imagenet_normalization"],
            training=False)

        unsupervised_dataset = load_data.get_unsupervised_dataset(
            X_unsup=X_unsup,
            resize_shape=config.TRAINING_PARAMS["resize_shape"],
            normalization=config.TRAINING_PARAMS["imagenet_normalization"])

        trainer = trainers.FixMatchTrainer()
        trainer.train(
            student_model=student_model,
            teacher_model=teacher_model,
            supervised_train_dataset=supervised_train_dataset,
            supervised_val_dataset=supervised_val_dataset,
            unsupervised_dataset=unsupervised_dataset,
            training_params=config.TRAINING_PARAMS,
            validation_params=config.VALIDATION_PARAMS)

        evaluator = evaluators.SupervisedEvaluator()
        targets, predictions = evaluator.predict(
            model=teacher_model,
            dataset=supervised_test_dataset,
            testing_params=config.TESTING_PARAMS)

        # Compute here metrics for evaluation on the test set :
        # results.append({
        #     "y_true": targets,
        #     "y_pred": predictions
        #     })

        # with open("./results/fixmatch.json", 'w') as f:
        #     json.dump(results, f)


if __name__ == "__main__":
    main()
