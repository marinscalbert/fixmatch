import torch.nn as nn
from torchvision.models import (
    resnet18,
    resnet34,
    resnet50,
    resnet101,
    resnet152)


def get_model_class(model_name):
    if model_name == "ResNet18":
        return ResNet18
    elif model_name == "ResNet34":
        return ResNet34
    elif model_name == "ResNet50":
        return ResNet50
    elif model_name == "ResNet101":
        return ResNet101
    elif model_name == "ResNet152":
        return ResNet152


class ResNet(nn.Module):

    def __init__(self, resnet_class, num_classes, pretrained=True):
        super(ResNet, self).__init__()
        self.pretrained = pretrained
        self.model = resnet_class(pretrained=self.pretrained)
        in_features = self.model.fc.in_features
        if num_classes == 2:
            out_features = 1
            self.model.fc = nn.Sequential(
                nn.Linear(in_features, out_features),
                nn.Sigmoid())
        else:
            out_features = num_classes
            self.model.fc = nn.Linear(in_features, out_features)

    def forward(self, x):
        return self.model(x)


class ResNet18(ResNet):

    def __init__(self, num_classes, pretrained=True):
        super(ResNet18, self).__init__(
            resnet_class=resnet18,
            num_classes=num_classes,
            pretrained=pretrained)


class ResNet34(ResNet):

    def __init__(self, num_classes, pretrained=True):
        super(ResNet34, self).__init__(
            resnet_class=resnet34,
            num_classes=num_classes,
            pretrained=pretrained)


class ResNet50(ResNet):

    def __init__(self, num_classes, pretrained=True):
        super(ResNet50, self).__init__(
            resnet_class=resnet50,
            num_classes=num_classes,
            pretrained=pretrained)


class ResNet101(ResNet):

    def __init__(self, num_classes, pretrained=True):
        super(ResNet101, self).__init__(
            resnet_class=resnet101,
            num_classes=num_classes,
            pretrained=pretrained)


class ResNet152(ResNet):

    def __init__(self, num_classes, pretrained=True):
        super(ResNet152, self).__init__(
            resnet_class=resnet152,
            num_classes=num_classes,
            pretrained=pretrained)
