import tqdm
import copy
import numpy as np
import torch
from torch import nn
from torch import optim
import torch.nn.functional as F
from torch.utils.data import DataLoader
from sklearn.utils.class_weight import compute_class_weight

import datasets
import losses
import metrics
import utils


class FixMatchTrainer():

    def __init__(self):
        pass

    def train_step(
            self,
            student_model,
            teacher_model,
            X_sup,
            y_sup,
            X_unsup_weak,
            X_unsup_strong,
            optimizer,
            training_params,
            class_weights,
            global_step):

        optimizer.zero_grad()

        logits_sup = student_model(X_sup)
        logits_weak = student_model(X_unsup_weak)
        logits_strong = student_model(X_unsup_strong)

        if training_params["use_class_weights"]:
            weights = class_weights.to(training_params["device"])
        else:
            weights = None

        # Compute different losses and apply one step of gradient descent
        sup_loss = F.cross_entropy(
            input=logits_sup,
            target=y_sup,
            weight=weights,
            reduction="mean")

        unsup_loss = training_params["lambda_unsupervised"] *\
            losses.FixMatchLoss(training_params["prob_threshold"])(
            logits_strong=logits_strong,
            logits_weak=logits_weak)
        loss = sup_loss+unsup_loss
        loss.backward()
        optimizer.step()
        self.update_teacher(
            student_model=student_model,
            teacher_model=teacher_model,
            alpha=training_params["ema_decay"],
            global_step=global_step)

        # Update running loss and metric
        with torch.no_grad():
            logits_sup_teacher = teacher_model(X_sup)
            pred_classes = torch.argmax(logits_sup_teacher, dim=-1)
            pred_classes = pred_classes.type(logits_sup_teacher.type())
            acc = metrics.Accuracy()(pred_classes, y_sup)

        return loss, sup_loss, unsup_loss, acc

    def eval_step(
            self,
            supervised_val_dataloader,
            teacher_model,
            validation_params,
            class_weights):

        logits = []
        targets = []
        with torch.no_grad():
            for X, y in supervised_val_dataloader:
                X = X.to(validation_params["device"])
                y = y.to(validation_params["device"])
                logit = teacher_model(X)
                targets.append(y)
                logits.append(logit)
            targets = torch.cat(targets).view(-1)
            logits = torch.cat(logits)

            if validation_params["use_class_weights"]:
                weights = class_weights.to(validation_params["device"])
            else:
                weights = None
            criterion = nn.CrossEntropyLoss(weight=weights)
            val_loss = criterion(logits, targets).item()

            pred_classes = torch.argmax(logits, dim=-1)
            pred_classes = pred_classes.type(logits.type())
            val_acc = metrics.Accuracy()(pred_classes, targets)

        return val_loss, val_acc

    def train(
            self,
            student_model,
            teacher_model,
            supervised_train_dataset,
            supervised_val_dataset,
            unsupervised_dataset,
            training_params,
            validation_params):

        # Create data loaders
        supervised_train_dataloader = DataLoader(
            supervised_train_dataset,
            batch_size=training_params["batch_size"],
            shuffle=training_params["shuffle"],
            num_workers=training_params["num_workers"])

        supervised_val_dataloader = DataLoader(
            supervised_val_dataset,
            batch_size=validation_params["batch_size"],
            shuffle=validation_params["shuffle"],
            num_workers=validation_params["num_workers"])

        unsupervised_dataloader = datasets.RandomBatchDataloader(
            unsupervised_dataset,
            batch_size=int(
                training_params["sup_unsup_batch_ratio"]*training_params["batch_size"]),
            shuffle=training_params["shuffle"],
            num_workers=training_params["num_workers"])

        # Move model to specified device
        teacher_model = teacher_model.to(training_params["device"])
        student_model = student_model.to(training_params["device"])

        # Create optimizer and lr_scheduler
        optimizer = optim.Adam(
            student_model.parameters(), lr=training_params["lr"])

        scheduler = optim.lr_scheduler.LambdaLR(
            optimizer=optimizer,
            lr_lambda=lambda x: utils.cosine_decay(
                x,
                lmax=training_params["lr"],
                lmin=training_params["min_lr"],
                decay_steps=training_params["cosine_decay_lr_steps"]),
            last_epoch=-1)

        # Get class weights if required
        if training_params["use_class_weights"]:
            class_weights = compute_class_weight(
                "balanced",
                np.unique(supervised_train_dataset.labels),
                supervised_train_dataset.labels).tolist()
            class_weights = torch.Tensor(class_weights)
        else:
            class_weights = None

        # Initialize variables for early stopping
        best_val_loss = np.inf
        best_state_dict = teacher_model.state_dict()
        val_loss_has_not_improved = 0

        # Training loop
        global_step = 0
        for epoch in range(training_params["epochs"]):
            student_model = student_model.train()
            teacher_model = teacher_model.train()

            pbar = tqdm.tqdm(
                supervised_train_dataloader,
                unit="batch",
                desc="{}/{} Epochs, loss: {}, sup_loss: {}, unsup_loss: {}, acc: {}".format(
                    epoch+1,
                    training_params["epochs"],
                    None,
                    None,
                    None,
                    None),
                total=len(supervised_train_dataloader))
            for X_sup, y_sup in pbar:
                X_sup = X_sup.to(training_params["device"])
                y_sup = y_sup.to(training_params["device"])
                X_unsup_weak, X_unsup_strong = next(unsupervised_dataloader)
                X_unsup_weak = X_unsup_weak.to(training_params["device"])
                X_unsup_strong = X_unsup_strong.to(training_params["device"])

                loss, sup_loss, unsup_loss, acc = self.train_step(
                    student_model=student_model,
                    teacher_model=teacher_model,
                    X_sup=X_sup,
                    y_sup=y_sup,
                    X_unsup_weak=X_unsup_weak,
                    X_unsup_strong=X_unsup_strong,
                    optimizer=optimizer,
                    training_params=training_params,
                    class_weights=class_weights,
                    global_step=global_step)

                pbar.set_description(
                    "Epoch {}/{}, loss: {:.3f}, sup_loss: {:.3f}, unsup_loss: {:.3f}, acc: {:.3f}".format(
                        epoch+1,
                        training_params["epochs"],
                        loss.item(),
                        sup_loss.item(),
                        unsup_loss.item(),
                        acc.item()))

            # Validation step
            student_model = student_model.eval()
            teacher_model = teacher_model.eval()

            val_loss, val_acc = self.eval_step(
                supervised_val_dataloader=supervised_val_dataloader,
                teacher_model=teacher_model,
                validation_params=validation_params,
                class_weights=class_weights)

            print("val_loss: {:.3f}, val_acc: {:.3f}%".format(
                val_loss,
                val_acc*100))

            # Step with learning rate scheduler
            scheduler.step(val_loss)

            # Do early stopping and reload best weights if necessary
            if val_loss <= best_val_loss:
                best_val_loss = val_loss
                val_loss_has_not_improved = 0
                best_state_dict = teacher_model.state_dict()
            else:
                val_loss_has_not_improved += 1

            if val_loss_has_not_improved == training_params[
                    "patience_early_stopping"]:
                break

            global_step += 1
        teacher_model.load_state_dict(best_state_dict)

    def update_teacher(
            self,
            student_model,
            teacher_model,
            alpha,
            global_step):
        alpha = min(1 - 1 / (global_step + 1), alpha)
        for ema_param, param in zip(
                teacher_model.parameters(), student_model.parameters()):
            if ema_param.requires_grad:
                ema_param.data.mul_(alpha).add_(param.data, alpha=1-alpha)
