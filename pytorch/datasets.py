import torch
from torch.utils.data import Dataset, DataLoader
from PIL import Image


class SupervisedDataset(Dataset):

    def __init__(
            self,
            img_files,
            labels,
            transform):
        super(SupervisedDataset, self).__init__()
        self.img_files = img_files
        self.labels = labels
        self.transform = transform

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, index):
        img = Image.open(self.img_files[index])
        img = self.transform(img)
        y = self.labels[index]
        y = torch.tensor(y, dtype=torch.long)
        return img, y


class TwoStreamUnsupervisedDataset(Dataset):

    def __init__(
            self,
            img_files,
            weak_transform,
            strong_transform):
        super(TwoStreamUnsupervisedDataset, self).__init__()
        self.img_files = img_files
        self.weak_transform = weak_transform
        self.strong_transform = strong_transform

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, index):

        img = Image.open(self.img_files[index])
        weak_img = self.weak_transform(img)
        strong_img = self.strong_transform(img)
        return weak_img, strong_img


class RandomBatchDataloader(DataLoader):

    def __init__(self, *args, **kwargs):
        super(RandomBatchDataloader, self).__init__(*args, **kwargs)
        self.dataset_iterator = super(RandomBatchDataloader, self).__iter__()

    def __iter__(self):
        return self

    def __next__(self):
        try:
            batch = next(self.dataset_iterator)
        except StopIteration:
            # Dataset exhausted, use a new iterator.
            self.dataset_iterator = super().__iter__()
            batch = next(self.dataset_iterator)
        return batch
