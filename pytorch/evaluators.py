import torch
from torch.utils.data import DataLoader


class SupervisedEvaluator():

    def __init__(self):
        pass

    def predict(
            self,
            model,
            dataset,
            testing_params):

        # Create test dataloader
        test_dataloader = DataLoader(
            dataset,
            batch_size=testing_params["batch_size"],
            shuffle=testing_params["shuffle"],
            num_workers=testing_params["num_workers"])

        targets = []
        logits = []
        with torch.no_grad():
            for x, y in test_dataloader:
                x = x.to(testing_params["device"])
                y = y.to(testing_params["device"])
                targets.append(y)

                logit = model(x)
                logits.append(logit)

            targets = torch.cat(targets)
            logits = torch.cat(logits)

            _, preds_classes = torch.max(logits, -1)

            targets_np = targets.cpu().detach().numpy().tolist()
            preds_classes_np = preds_classes.cpu().detach().numpy().tolist()

        return targets_np, preds_classes_np
