import torch
import torch.nn as nn
import torch.nn.functional as F


class FixMatchLoss(nn.Module):

    """Class implementing the unsupervised loss term used in the FixMatch paper

    Attributes:
        prob_threshold (float): Probability threshold hyperparameter used in
            the FixMatch paper.
    """

    def __init__(
            self,
            prob_threshold):
        """Constructor of the class FixMatchLoss

        Args:
            prob_threshold (float): Probability threshold
            hyperparameter used in the FixMatch paper.
        """
        super(FixMatchLoss, self).__init__()
        self.prob_threshold = prob_threshold

    def forward(self, logits_strong, logits_weak):
        """Computes the FixMatch unsupervised loss term


        Args:
            logits_strong (torch.Tensor): Predictions from the student model.
            logits_weak (torch.Tensor): Predictions from the teacher model.

        Returns:
            torch.Tensor: Loss value.
        """
        probs = nn.Softmax(dim=-1)(logits_weak)
        max_probs, targets = torch.max(probs, dim=-1)
        mask = max_probs.ge(self.prob_threshold).float()
        loss = (F.cross_entropy(logits_weak, targets,
                                reduction='none') * mask).mean()
        return loss
