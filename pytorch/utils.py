import numpy as np


def ema_decay(t, alpha=0.999):
    alpha = min(1 - 1 / (t + 1), alpha)
    return alpha


def linear_ramp(t, t_start, t_end):
    if t < t_start:
        return 0
    else:
        return min((t-t_start)/(t_end-t_start), 1)


def step_ramp(t, t_start):
    if t < t_start:
        return 0
    else:
        return 1


def cosine_decay(x, lmax, lmin, decay_steps):
    if x <= decay_steps:
        new_lr = ((lmax-lmin)/2)*np.cos(x*np.pi/decay_steps)+(lmax+lmin)/2
    else:
        new_lr = lmin
    print("New learning rate: {}".format(new_lr))
    return new_lr
