"""
SUPERVISED_DATA_DIR should be organized such as the following example:

SUPERVISED_DATA_DIR
            ├── label1
            │   ├── img1.jpeg
            │   ├── img2.jpeg
            │   ├── img3.jpeg
            │   └── img4.jpeg
            ├── label2
            │   ├── img1.jpeg
            │   ├── img2.jpeg
            │   └── img3.jpeg
            └── label3
                ├── img1.jpeg
                ├── img2.jpeg
                ├── img3.jpeg
                ├── img4.jpeg
                └── img5.jpeg

UNSUPERVISED_DATA_DIR should be organized such as the following example:

UNSUPERVISED_DATA_DIR
            ├── img1.jpeg
            ├── img2.jpeg
            ├── img3.jpeg
            ├── img4.jpeg
            └── img5.jpeg
"""
SUPERVISED_DATA_DIR = "/Users/scalbertmarin/supervised_data_dir/"
UNSUPERVISED_DATA_DIR = "/Users/scalbertmarin/unsupervised_data_dir/"
