import torch

# Configuration for supervised data splits
SPLIT_SEED = 0
N_SPLITS = 1
TRAIN_SIZE = 0.8
VAL_SIZE = 0.1
TEST_SIZE = 0.1

# Configuration model name
MODEL_NAME = "ResNet50"
PRETRAINED = True
CLASSES = 5

# Configuration for training
TRAINING_PARAMS = {
    "imagenet_normalization": PRETRAINED,
    "resize_shape": (128, 128),
    "lr": 1e-2,
    "min_lr": 0,
    "cosine_decay_lr_steps": 200,
    "patience_early_stopping": 10,
    "device": torch.device("cuda") if torch.cuda.is_available() else "cpu",
    "epochs": 200,
    "batch_size": 32,
    "shuffle": True,
    "num_workers": 8,
    "use_class_weights": True,
    "sup_unsup_batch_ratio": 4,
    "lambda_unsupervised": 1,
    "prob_threshold": 0.90,
    "ema_decay": 0.995}

# Configuration for validation
VALIDATION_PARAMS = {
    "imagenet_normalization": PRETRAINED,
    "resize_shape": (128, 128),
    "device": torch.device("cuda") if torch.cuda.is_available() else "cpu",
    "batch_size": 32,
    "shuffle": False,
    "num_workers": 8,
    "use_class_weights": True
}

# Configuration for testing
TESTING_PARAMS = {
    "imagenet_normalization": PRETRAINED,
    "resize_shape": (128, 128),
    "device": torch.device("cuda") if torch.cuda.is_available() else "cpu",
    "batch_size": 32,
    "shuffle": False,
    "num_workers": 8,
    "use_class_weights": True
}
