class Accuracy():

    def __init__(self):
        pass

    def __call__(self, input, target):
        iflat = input.view(-1)
        tflat = target.view(-1)
        num_corrects = (iflat == tflat).type(input.type()).sum()
        num_total = input.numel()
        acc = num_corrects/num_total
        return acc
