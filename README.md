# FixMatch: Simplifying Semi-Supervised Learning with Consistency and Confidence

This project provides a Tensorflow and a Pytorch implementations of the semi-supervised framework detailed in this
<a href="https://arxiv.org/pdf/2001.07685.pdf">paper</a>.

<p align="center">
<img src="https://miro.medium.com/max/2914/1*nwxClQDqWakknowNAqTobQ.png" alt="Diagram of FixMatch" scale="1"/>
</p>